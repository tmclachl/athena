# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

AntiKt4EMPFlowFEJetsCPContent = [
"Kt4EMPFlowFEEventShape",
"Kt4EMPFlowFEEventShapeAux.Density",
"AntiKt4EMPFlowFEJets",
"AntiKt4EMPFlowFEJetsAux.pt.eta.phi.m.JetConstitScaleMomentum_pt.JetConstitScaleMomentum_eta.JetConstitScaleMomentum_phi.JetConstitScaleMomentum_m.NumTrkPt500.SumPtTrkPt500.NumChargedPFOPt500.SumPtChargedPFOPt500.EnergyPerSampling.ActiveArea4vec_eta.ActiveArea4vec_m.ActiveArea4vec_phi.ActiveArea4vec_pt.DetectorEta.DetectorY.FracSamplingMax.FracSamplingMaxIndex.GhostTrack.Jvt.JVFCorr.JvtRpt.NumTrkPt1000.NumChargedPFOPt1000.TrackWidthPt1000.ChargedPFOWidthPt1000.GhostMuonSegmentCount.PartonTruthLabelID.ConeTruthLabelID.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor.Timing",
"PrimaryVertices",
"PrimaryVerticesAux.vertexType" 
]
